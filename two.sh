#!/bin/bash
set -e

a="abc"

if [ $a=="abc" ]
then
    source 'lib.trap.sh'
    error=1
fi

if [ $error==1 ]
then
    echo "error $error thrown"
fi

